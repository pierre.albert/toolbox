# Introduction
This repository contains scripts for different specific tasks.
audio_corpus_preprocess.py: preprocess audio files
review_analyse_search.py: process export files from online scientific DBs and merge them

# Installation
## audio_corpus_preprocess.py
- python3 (>=3.5)
- ffmpeg (http://ffmpeg.org, must be in your PAT, i.e. you can run
"ffmpeg" in your terminal)
- pydub (pip install pydub)
## review_analyse_search.py
- python3 (>=3.5)
- numpy
- pandas
- nlotk

# Running audio_corpus_preprocess.py
How to use it:
- set the "corpus_path" variable to the location of all your .wav files (they
have to be in the folder, not in subfolders
- set the "output_path" variable or create a folder called "out_sil" in
the local folder where you've put the python script
- run (python audio_corpus_preprocess.py)

if you find that the silences that were found contains voices:
- set the "sil_tolerance_db" lower (35,40, ...)
if you find that no silences were found:
- set the "sil_tolerance_db" higher (20, ...)


# Running review_analyse_search.py
export the results from the website (do not forget to include abstracts):
.bib: acm, cochrane, webofscience
.csv: medline, ieee, embase
- put each exported file in one folder
- go into the folder 
- run review_analyse_search.py