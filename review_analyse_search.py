#!/usr/bin/env python3
# encoding: utf-8
# author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2017
#
# process results of main medical electronic databases
# Includes logger
#

# imports
import sys
import os
from pathlib import Path
import csv

# logging tools lib
import logging

import re

# math/vis
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from nltk.metrics import distance

from datetime import datetime, timedelta, date


class ReviewAnalysis(object):
	"""Program main class
	
	Analysis of review search results
	"""
	# class variables
	logger=None
	corpus = None
	total_length = None
	x = None
	y = None
	csv_dataset = None
	dataset = None
	databases = None
	duplicate = None
	duplicate_list = None
	
	def __init__(self):
		"""Initialise class related properties:
		* configure and set up the logger
		
		Parameters
		----------
		
		None
		"""
		# set up logger
		self.logger = logging.getLogger("ReviewAnalysis")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		
		self.csv_dataset=[]
		self.databases=[]
		self.dataset={}
		self.duplicate_list=[]
		
		self.x=[]
		self.y=[]
		
		self.duplicate=0
		return
		
	def default_init(self):
		"""Load default databases (every compatible ones)
		Set databases to load the data from, with corresponding separator.
		Medical DB:
		* Cochrane
		* Embase
		* Medline
		ML DB:
		* IEEE
		* ACM
		
		Parameters
		----------
		
		None
		"""
		# self.databases = {"cochrane":"¤", "embase":",", "medline":","}	# medical DB
		self.databases = {"cochrane":"¤", "embase":"¤", "medline":",", "ieee":",", "acm": ",", "webofscience":",", "pubmed":","}	# all supported DB
		# self.databases = {"embase":","}	# all supported DB
		# self.databases = {"acm": ","}
		
		return
		
	def load_data(self, formats=["csv", "bib"]):
		"""
		Load data from file exports
		
		Parameters
		----------
		
		:param formats: set of file format to extract references from
		:type formats: string array
		
		"""
		ct_global = 0
		csv_dataset=[]
		data_path = Path(".")
		files = [file for file in data_path.iterdir() if file.is_file()]
		# search bib files
		if("bib" in formats):
			bib_files = list(data_path.glob('*.bib'))
			for bib_path in bib_files:
				for db_name in self.databases:
					if(db_name in bib_path.stem.lower()):
						self.logger.debug("processing bib file: {}".format(bib_path))
						bib_file = bib_path.open('r', encoding="utf-8")
						if(db_name == "acm"):
							ct = self.extract_acm_bib(bib_file)
						elif(db_name == "cochrane"):
							ct = self.extract_cochrane_bib(bib_file)
						elif(db_name == "webofscience"):
							ct = self.extract_webofscience_bib(bib_file)
						self.logger.debug("{} (bib): # of records: {}".format(db_name, ct))
						self.logger.debug("{} (bib): # of duplicates detected: {}".format(db_name, self.duplicate))
						ct_global+=ct
		# search csv files
		if("csv" in formats):
			csv_files = list(data_path.glob('*.csv'))
			for csv_path in csv_files:
				found = False
				if (not found):
					for db_name in self.databases:
						if(db_name in csv_path.stem.lower()):
							found = True
							self.logger.debug("processing csv file: {}".format(csv_path))
							with csv_path.open('r', encoding="utf-8") as csv_file:
								ct=0
								self.duplicate=0
								if(db_name == "cochrane"):
									ct = self.extract_cochrane(csv_file)
								elif(db_name == "embase"):
									ct = self.extract_embase(csv_file)
								elif(db_name == "medline"):
									ct = self.extract_medline(csv_file)
								elif(db_name == "ieee"):
									ct = self.extract_ieee(csv_file)
								elif(db_name == "pubmed"):
									ct = self.extract_pubmed(csv_file)
								if(db_name == "acm"):
									ct = self.extract_acm(csv_file)
								self.logger.debug("{}: # of records: {}".format(db_name, ct))
								self.logger.debug("{}: # of duplicates detected: {}".format(db_name, self.duplicate))
								self.logger.debug("Cumulated # of unique records: {}".format(len(self.dataset)))
								self.logger.debug("Cumulated # of records: {}".format(len(self.csv_dataset)))
								ct_global+=ct
			self.logger.debug("total # of records: {}".format(ct_global))
			self.logger.debug("total unique # of records: {}".format(len(self.dataset)))
		return
		
	def add_article(self, art_key):
		""" Add article to the list of found ones.
		append refs to existing articles if already referenced
		
		Parameters
		----------
		
		:param art_key: unique identifier of the article (title)
		:type art_key: string
		
		:return: True if the article was added (i.e. was a new one)
		:rtype: boolean
		"""
		added=False
		try:	# add to list
			self.dataset[art_key].append(len(self.csv_dataset)-1)
			self.logger.log(1,"Duplicate found: {}".format(art_key))
			self.duplicate+=1
		except KeyError:	# new entry
			self.dataset[art_key] = []
			self.dataset[art_key].append(len(self.csv_dataset)-1)
			# self.logger.debug("New found: {}".format(art_key))
			added=True
		return added
	
	def dataset_detect_double(self, edit_dist=10):
		"""Search for duplicates in the list
		Search for articles with a similar edit distance.
		
		Parameters
		----------
		
		:param edit_dist: default 1, distance to look for similarity
		:type edit_dist: integer
		
		"""
		self.logger.debug("length of the dataset: {}".format(len(self.dataset)))
		if(edit_dist>0):	# edit distance was provided
			first_keys = list(self.dataset.keys())
			for i in range(0, len(first_keys)):
				# print((i/len(first_keys))*100)
				for j in range(i+1, len(first_keys)):
					el_1 = first_keys[i]
					el_2 = first_keys[j]
					# if("2008" in el_1 and "2007" in el_2):
						# print()
				# print(distance.edit_distance(element, art_key), element,art_key)
					edit_distance_a = distance.edit_distance(el_1, el_2)
					if(edit_distance_a <= edit_dist):
						self.logger.debug("Potentially similar. dist: {} 1: {} 2: {}".format(edit_distance_a, el_1, el_2))
				# self.logger.debug("Potentially similar. dist: {} 1: {} 2: {}".format(edit_distance_a, el_1, el_2))
		# self.logger.debug("length of the dataset: {}".format(len(self.dataset)))
		return
	
	def extract_cochrane(self, csv_file):
		"""Extract reference list from Cochrane export file
		extract from Cochrane CSV file format
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		db_name="cochrane"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		for row in csv_reader:
			if(row[0][0] !='#'):
				self.csv_dataset.append([db_name] + row)
				for item in row:
					if(item[0:2] == "TI"):
						title = item[4:].lower()
						self.add_article(title)
						break
				ct+=1
		return ct
		
	def extract_pubmed(self, csv_file):
		"""Extract reference list from Pubmed export file
		extract from Cochrane CSV file format
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		db_name="pubmed"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		row_num=0
		for row in csv_reader:
			if(row_num>0):
				self.csv_dataset.append([db_name] + row)
				for item in row:
					title = item[:-1].lower()
					self.add_article(title)
					break
				ct+=1
			row_num+=1
		return ct
		
	def extract_cochrane_bib(self, bib_file):
		"""Extract reference list from Cochrane export file (preprocessing required)
		extract from Cochrane bibtex file format
		Preprocessing:
		* replace author by authors
		* replace review groups by review_groups
		* replace publication type by publication_type
		
		Parameters
		----------
		
		:param bib_file: File to process
		:type bib_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		added_ct=0
		is_added=False
		db_name="cochrane"
		from pybtex.database import parse_file
		biblio = parse_file(bib_file)
		# each entry
		for item in biblio.entries.keys():
			# extract info to csv
			csv_array = self.pybtex_entry_to_csv(biblio.entries[item])
			# append extracted csv to the list of articles
			item_array = [db_name] + csv_array
			self.csv_dataset.append(item_array)
			title=biblio.entries[item].fields["title"].lower()
			is_added = self.add_article(title)
			if (is_added is True):
				added_ct+=1
			ct+=1
		self.logger.debug("Extracted: {:}, new: {:}".format(ct, added_ct))
		return ct
		
	def extract_embase(self, csv_file):
		""" extract EMBASE csv format
		Need to pre-format EMBASE excel for clean csv:
		* in Excel replace "," with "¤"
		* save in csv
		* convert to UTF-8
		* replace \r\n(\d+,) with \t\1
		* remove \r\n and \n
		* replace \t with \n
		Make sure that the first two lines are:
		* Copyright [...]
		* ORN,VN,[...]
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		db_name="embase"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		header_count=0
		for row in csv_reader:
			if(header_count >= 2):
				self.csv_dataset.append([db_name] + row)
				# title=row[11].lower()	# old format
				title=row[6].lower()	# 2018-11
				title=row[7].lower()	# 2019-08
				title=title[:-1]	# trim last "."
				self.add_article(title)
				ct+=1
			header_count+=1
		return ct
		
	def extract_medline(self, csv_file):
		"""Extract reference list from Medline export file
		extract from Medline CSV file format
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		db_name="medline"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		header_count=0
		for row in csv_reader:
			if(header_count >= 1):
				self.csv_dataset.append([db_name] + row)
				title=row[0].lower()
				title=title[:-1]	# trim last "."
				self.add_article(title)
				ct+=1
			header_count+=1
		return ct
		
	def extract_ieee(self, csv_file):
		"""Extract reference list from IEEE export file
		extract from IEEE CSV file format
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		db_name="ieee"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		header_count=0
		for row in csv_reader:
			if(header_count >= 1):	# discard header
				self.csv_dataset.append([db_name] + row)
				title=row[0].lower()#[1:-1]
				self.add_article(title)
				ct+=1
			header_count+=1
		return ct
		
	def extract_acm_bib(self, bib_file):
		"""Extract reference list from ACM export file
		extract from ACM bibtex file format
		
		Parameters
		----------
		
		:param bib_file: File to process
		:type bib_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		added_ct=0
		is_added=False
		db_name="acm"
		from pybtex.database import parse_file
		biblio = parse_file(bib_file)
		# each entry
		for item in biblio.entries.keys():
			# extract info to csv
			csv_array = self.pybtex_entry_to_csv(biblio.entries[item])
			# append extracted csv to the list of articles
			item_array = [db_name] + csv_array
			self.csv_dataset.append(item_array)
			title=biblio.entries[item].fields["title"].lower()
			is_added = self.add_article(title)
			if (is_added is True):
				added_ct+=1
			ct+=1
		self.logger.debug("Extracted: {:}, new: {:}".format(ct, added_ct))
		return ct
		
	def extract_acm(self, csv_file):
		"""Extract reference list from ACM export file
		extract from ACM CSV file format
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		# return 0
		ct=0
		empty_ct=0
		added_ct=0
		is_added=False
		db_name="acm"
		csv_reader = csv.reader(csv_file, delimiter=self.databases[db_name])
		header_count=0
		for row in csv_reader:
			if(header_count >= 1):	# discard header
				# clean corrupted characters
				row[6]= row[6].replace("\&", "&")
				match_res = re.finditer("\\&\\#(\d+);", row[6])
				if(match_res is not None):
					for it in match_res:
						# print(it.group(1))
						el=it.group(1)
						row[6]=row[6].replace("&#"+el+";", chr(int(el)))
						# print(row[6])
				self.csv_dataset.append([db_name] + row)
				title=row[6].lower()
				# filter references without titles (generic refs to proceedings)
				if(len(title)>0):
					is_added = self.add_article(title)
					if (is_added is True):
						added_ct+=1
				else:
					# self.logger.debug("No title.")
					empty_ct+=1
				ct+=1
			header_count+=1
		self.logger.debug("Extracted: {:}, empty titles: {:}, new: {:}".format(ct, empty_ct, added_ct))
		return ct
		
	def extract_webofscience_bib(self, bib_file):
		"""Extract reference list from webofscience export file
		extract from webofscience bibtex file format
		
		Parameters
		----------
		
		:param bib_file: File to process
		:type bib_file: File
		
		:return: number of references found
		:rtype: integer
		"""
		ct=0
		added_ct=0
		is_added=False
		db_name="webofscience"
		from pybtex.database import parse_file
		biblio = parse_file(bib_file)
		# each entry
		for item in biblio.entries.keys():
			# extract info to csv
			csv_array = self.pybtex_entry_to_csv(biblio.entries[item], trim=True)
			# append extracted csv to the list of articles
			item_array = [db_name] + csv_array
			self.csv_dataset.append(item_array)
			title=biblio.entries[item].fields["title"].lower()
			title=title[1:-1]
			is_added = self.add_article(title)
			if (is_added is True):
				added_ct+=1
			ct+=1
		self.logger.debug("Extracted: {:}, new: {:}".format(ct, added_ct))
		return ct
		
	def pybtex_entry_to_csv(self, entry, trim=False):
		"""Convert a pybtex object to a csv line
		* clean corrupted chars
		
		Parameters
		----------
		
		:param entry: pybtex entry to process
		:type entry: pybtex
		
		:return: CSV array
		:rtype: string array
		
		
		"""
		# key, [field, field_value]+
		# from pybtex.database import BibliographyData
		# bd = BibliographyData(entries=[entry.key, entry])
		csv_array = [entry.key]
		for dict_it in entry.fields:
			csv_array.append(dict_it)
			content = entry.fields[dict_it]
			# TODO find cleaner way
			# fix corrupted characters ("\&\#38" => "&")
			match_res = re.finditer("\\\&\\\#(\d+);", content)
			if(match_res is not None):
				for it in match_res:
					el=it.group(1)
					content=content.replace("\&\#"+el+";", chr(int(el)))
				content=content.replace("\&", "&")
				entry.fields[dict_it] = content
			if(trim is False):
				csv_array.append(content)
			else:
				csv_array.append(content[1:-1])
		for dict_it in entry.persons:
			csv_array.append(dict_it)
			# if(trim is False):
			csv_array.append(entry.persons[dict_it])
			# else:
				# csv_array.append(entry.persons[dict_it][1:-1])
		return csv_array
		
	def write_art(self, output_file="./out.csv"):
		"""Write reference list to a file
		Write CSV file: 
		* delimiter: ";"
		* file: "out.csv" (local directory)
		
		Parameters
		----------
		
		:param output_file: path to output file
		:type output_file: string
		"""
		output_path = Path(output_file)
		ct=0
		with output_path.open('w', encoding='utf-8') as output_csv:
			csv_writer = csv.writer(output_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
			for art in self.dataset:
				row = self.csv_dataset[self.dataset[art][0]]
				towrite=[art] + row
				csv_writer.writerow(towrite)
				ct+=1
		self.logger.debug("saved {} references".format(ct))
		return
		
	def remove_list_csv(self, csv_file, out_list=None):
		"""Remove reference found in the given file
		Delete from reference list all references listed in the given file
		
		Parameters
		----------
		
		:param csv_file: File to process
		:type csv_file: File
		
		:param out_list: path of the file to write missing references
		:type out_list: File
		
		:return: number of references found
		:rtype: integer
		"""
		# return 0
		ct=0
		ct_d=0
		empty_ct=0
		loaded_ct=0
		is_added=False
		csv_file_o = Path(csv_file).open('r', encoding="utf-8")
		csv_reader = csv.reader(csv_file_o, delimiter=";")
		header_count=0
		self.logger.debug("Processing {}".format(csv_file))
		if(out_list is not None):
			out_file = Path(out_list).open('a', encoding="utf-8")
		for row in csv_reader:
			if(len(row) == 0):	# filter empty rows
				continue
			elif(row[0][0] == "#"):	# filter comments
				continue
			title = row[0]
			loaded_ct+=1
			if(title[-1] == "."):	# fix old format to new one
				title = title[:-1]
			if(title in self.dataset):	# remove found reference
				del self.dataset[title]
				ct+=1
				self.duplicate_list.append(title)
			elif(title not in self.duplicate_list):	# new reference not in the current reference list
				self.logger.debug("Reference not found: {:}".format(title))
				out_file.write(title+";"+csv_file+"\n")	# save referece
			else:	# reference was previously removed
				ct_d+=1
		out_file.close()
		self.logger.debug("Processed {} references".format(loaded_ct))
		self.logger.debug("Removed {} references ({} already found)".format(ct, ct_d))
		return ct
		
if __name__ == "__main__":
	dfg=ReviewAnalysis()
	dfg.default_init()
	dfg.load_data()	# search and load all DB files
	# search duplicates with typos (takes a lot of time)
	# self.dataset_detect_double()
	out_not_found_list = "./missing_list.txt"
	with open(out_not_found_list, 'w+') as f:
		pass
	# dfg.remove_list_csv("../2018-07/out.csv", "./missing_list.txt")	# remove list of references (e.g. previously analysed)
	dfg.remove_list_csv("../out_ACCEPT.csv", "./missing_list.txt")	# remove list of references previously accepted
	dfg.write_art()	# write list of reference into a csv file