#!/usr/bin/env python3
# encoding: utf-8
# author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2019
#
# 
# Includes logger
#

# imports
import sys
import os

# logging tools lib
import logging

import timeit
import cProfile
import pstats

import re

from pathlib import Path
from collections import Counter
import csv


class ConvertAudioAnalysisFormats(object):
	"""Program main class
	
	Converter for audio analysis files to other formats
	"""
	# class variables
	logger=None
	internal_data=None
	
	
	def __init__(self):
		"""Initialise class related properties:
		* configure and set up the logger
		
		Parameters
		----------
		
		None
		"""
		# set up logger
		self.logger = logging.getLogger("ConvertAudioAnalysisFormats")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		return
		
	def compute_frame(self, frame_array):
		# self.logger.debug("in {}".format(frame_array))
		data = Counter(frame_array)
		ret = max(frame_array, key=data.get)
		# self.logger.debug("out{}".format(ret))
		return ret
	
	def conv_SR_vector(self, SR_file, sampling_frq, step=0.01):
		"""
		
		step	# time step, in ms
		"""
		annot_array = []
		ret = None
		ct_stop=0
		frame_length=sampling_frq*step
		sample_num=0
		sample_frame=0
		frame=[]
		time = 0
		prev_frame = -1
		cur_start=0
		frame_num=0
		try:
			with open(SR_file, "r") as vector_file:
				# column frames = vector_file
				
				line = vector_file.read(2)
				while(line):
				# content = vector_file.read()
				# frames = re.split(content, ",")
				# process time 418s
				# for line in frames:
					if (sample_frame < frame_length):
						frame.append(line[0])
					else:
						frame_num+=1
						# process frame
						res = self.compute_frame(frame)
						# self.logger.debug("prev/cur: {} {}".format(prev_frame, res))
						if(prev_frame != res):
							stop = sample_num/frame_length
							# self.logger.debug("new : {}".format([prev_frame, cur_start, stop]))
							annot_array.append([prev_frame, cur_start/100, stop/100])
							prev_frame=res
							#start new frame
							# cur_start=(sample_num+1)/frame_length
							cur_start=(sample_num)/frame_length
						# reset
						sample_frame=0
						frame=[line[0]]
					sample_num+=1
					sample_frame+=1
					line = vector_file.read(2)
					# entries+=1
			print(sample_num)
			annot_array = annot_array[1:]
		except KeyboardInterrupt:
			pass
		return sample_num, annot_array
		
	def write_csv(self, annot_array, file_output):
		with file_output.open("w", newline='') as file_out:
			# csv_writer = csv.DictWriter(file_out, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, extrasaction='ignore', fieldnames=["annotation", "start_time", "end_time"])
			csv_writer = csv.writer(file_out, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
			csv_writer.writerows(annot_array)
		return
	
if __name__ == "__main__":
	file_in = Path(r"C:\work\datasets\INCA\CUSCO recordings\Nino-Pierre\vadOutputClean.csv")
	file_out = Path(r"C:\work\datasets\INCA\CUSCO recordings\Nino-Pierre\vad_frame.csv")
	sampling_frq = 44100
	# init 
	caaf = ConvertAudioAnalysisFormats()
	start_time = timeit.default_timer()
	samples, res = caaf.conv_SR_vector(file_in, sampling_frq)
	search_duration = timeit.default_timer() - start_time
	caaf.write_csv(res, file_out)
	eff_fps=len(res)/search_duration
	eff_sps=samples/search_duration
	print("In {:}s".format(search_duration))
	print("Samples: {}".format(samples))
	print("Efficiency: {:,f} frame per second".format(eff_fps))
	print("Efficiency: {:,f} sample per second".format(eff_sps))

	
	
	
	