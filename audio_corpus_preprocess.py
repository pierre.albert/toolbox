#!/usr/bin/env python3
# encoding: utf-8
# author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2019
#
# tool for preprocessing of an audio corpus
# silence detection and selection
#
# requirements:
# - ffmpeg
# - sox


import os
# logging tools lib
import logging

from pathlib import Path

from pydub import AudioSegment, playback

# import threading
import subprocess
import shlex	# to split arguments correctly
# regex
import re

import datetime
from matplotlib import pyplot as plt

from time import process_time 

# 
# Program main class
# 
class AudioCorpusPreprocess(object):
	logger=None
	audio_file=None
	audio_signal=None
	corpus_path=None
	output_path=None
	corpus=None
	stat_array=None
	
	def __init__(self):
		"""Initialise class related properties:
		* configure and set up the logger
		* initiate vars
		
		Parameters
		----------
		
		None
		"""
		log_level = logging.DEBUG
		# set up logger
		self.logger = logging.getLogger("AudioCorpusPreprocess")
		self.logger.setLevel(log_level)
		ch = logging.StreamHandler()
		ch.setLevel(log_level)
		formatter = logging.Formatter('%(asctime)s - %(name)s %(funcName)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# init vars
		# corpus
		self.corpus_info={}
		return
		
	def set_output_path(self, output_path):
		"""Set the path for output files
		
		Parameters
		----------
		:param output_path: path to directory to write genrerated files
		:type output_path: Path
		"""
		if(not output_path.is_dir()):
			output_path.mkdir()
		self.output_path=output_path.resolve()
		return
		
	def configure_silence_detection(self, sil_tolerance_db=-60, sil_dur=2, sil_tolerance_ratio=None):
		"""Set the path for output files
		
		Parameters
		----------
		:param sil_tolerance_db: threshold for silence detection (positive values are set to negative). Any sound below the threshold will be tagged as silence.
		:type sil_tolerance_db: integer
		
		:param sil_dur: minimum length for silence detection. Silence of shorter duration that set will be ignored.
		:type sil_dur: integer
		
		:param sil_tolerance_ratio: ratio for silence detection (replaces value set with sil_tolerance_db). 
		:type sil_tolerance_ratio: float
		"""
		sil_tolerance_db= -1*abs(sil_tolerance_db)
		self.sil_tolerance_db=sil_tolerance_db
		self.sil_tolerance_ratio=sil_tolerance_ratio
		self.sil_dur=sil_dur
		return
		
	def load_audio_file(self, file_path):
		"""Load the specified audio file (wav only) into an AudioSegment object.
		
		Parameters
		----------
		:param file_path: path to a wav file
		:type file_path: Path
		"""
		self.audio_file=file_path
		self.audio_signal = AudioSegment.from_file(file_path.as_posix(), format="wav")
		return
		
	def detect_DC_offect_ffmpeg(self):
		"""Detect the DC offset of the active audio file using FFMPEG.
		
		Parameters
		----------
		
		:return: DC offset values for each channel and overall: {"1", "2", "overall"}
		:rtype: dictionary
		"""
		stat_array = {}
		# if file_path Path:
			# file_path_str = file_path.as_posix()
			
		ffmpeg_cmd = "ffmpeg -i \"{:}\" -af astats -f null -".format(self.audio_file.as_posix()) # "-i" input_file, "-af" audio filters "astats" audio statistics, "-f" format (mandatory, set to null since not used) "-" output (not set since not used)
		args = shlex.split(ffmpeg_cmd)
		self.logger.debug("DC offset detection {:}".format(str(args)))
		p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		if(p.returncode == 0):
			# print(p.stdout)
			# print(p.stderr)
			ffmpeg_output = p.stderr.decode('utf-8')
			for line in ffmpeg_output.split('\n'):
				line = line.strip()	# removes unwanted chars, e.g. '\r' on windows
				if("Overall" in line):
					channel = "overall"
					stat_array[channel] = {}
				if("Channel:" in line):
					channel = str(re.search("Channel: (.+)", line).group(1))
					stat_array[channel] = {}
				if("DC offset" in line):
					offset = float(re.search("DC offset: (.+)", line).group(1))
					stat_array[channel]["DC_offset"] = offset
		return stat_array
		
	def detect_DC_offect_AudioSegment(self):
		"""Detect the DC offset of the active audio file using AudioSegment.
		
		Parameters
		----------
		
		:return: DC offset values for each channel and overall: {"1", "2", "overall"}
		:rtype: dictionary
		"""
		stat_array = {"1": {}, "2": {}, "overall": {}}
		stat_array["1"]["DC_offset"] = self.audio_signal.get_dc_offset(channel=1)
		stat_array["2"]["DC_offset"] = self.audio_signal.get_dc_offset(channel=2)
		stat_array["overall"]["DC_offset"] = (stat_array["1"]["DC_offset"]+stat_array["2"]["DC_offset"])/2
		return stat_array
		
	def normalise_offset(self, offset_l, offset_r=None):
		"""Normalise the DC offset of the active audio file using provided correction offset.
		
		Parameters
		----------
		
		:param offset_l: correction offset for the left channel (or mono if offset_r is None)
		:type offset_l: float
		
		:param offset_r: correction offset for the right channel
		:type offset_r: float
		"""
		self.audio_signal.remove_dc_offset(channel=1, offset=offset_l)
		off_type = "mono"
		if(offset_r!=None):
			self.audio_signal.remove_dc_offset(channel=2, offset=offset_r)
			off_type = "stereo"
		self.logger.debug("DC offset normalised {}".format(off_type))
		return
		
	def normalise_RMS(self, norm_dB=-20):
		"""Normalise the volume of the active audio file to the provided level.
		Unit: Decibels relative to full scale (dBFS)
		RMS normalisation:
		* check mean level
		* compute offset
		* apply offset to every frame
		
		Parameters
		----------
		
		:param volu_norm: volume
		:type volu_norm: float
		"""
		audio_signal = self.audio_signal
		self.audio_signal = self.normalise_RMS_segment(audio_signal)
		return
		
	def normalise_RMS_segment(self, audio_signal, norm_dB=-20):
		"""Normalise the volume of the active audio file to the provided level.
		RMS normalisation:
		* check mean level
		* compute offset
		* apply offset to every frame
		Return normalised signal
		
		Parameters
		----------
		
		:param audio_signal: audio signal
		:type audio_signal: AudioSegment
		
		:param norm_dB: volume
		:type norm_dB: float
		
		return AudioSegment
		"""
		# ffmpeg -i input.wav -filter:a volumedetect -f null /dev/null
		
		self.logger.debug("Detected volume: {:.2f}dB ".format(audio_signal.dBFS))
		# self.logger.debug("Detected volume: {:.2f} rms".format(audio_signal.rms))
		self.logger.debug("Detected volume: {:.2f}dB max".format(audio_signal.max_dBFS))
		dBFS_offset = norm_dB - audio_signal.dBFS
		self.logger.debug("Correction offset: {:.2f}dB ".format(dBFS_offset))
		audio_signal = audio_signal.apply_gain(dBFS_offset)
		self.logger.debug("Corrected volume {:.2f}dB ".format(audio_signal.dBFS))
		# self.logger.debug("Corrected volume {:.2f} rms".format(audio_signal.rms))
		self.logger.debug("Corrected volume {:.2f}dB max".format(audio_signal.max_dBFS))
		return audio_signal
	
	def detect_silence(self, sil_tolerance_db=None, sil_dur=None, sil_tolerance_ratio=None):
		"""Detect silences in the active audio file using FFMPEG
		
		Parameters
		----------
		
		:param sil_tolerance_db: threshold for silence detection (positive values are set to negative). Any sound below the threshold will be tagged as silence.
		:type sil_tolerance_db: integer
		
		:param sil_dur: minimum length for silence detection. Silence of shorter duration that set will be ignored.
		:type sil_dur: integer
		
		:param sil_tolerance_ratio: ratio for silence detection (replaces value set with sil_tolerance_db). Default ratio in ffmpeg=0.001
		:type sil_tolerance_ratio: float
		
		:return: list of silences: (time_start, time_end, duration)
		:rtype: list
		"""
		if(sil_tolerance_db is None):
			sil_tolerance_db=self.sil_tolerance_db
		if(sil_tolerance_ratio is None):
			sil_tolerance_ratio=self.sil_tolerance_ratio
		if(sil_dur is None):
			sil_dur=self.sil_dur
		ffmpeg_cmd = "ffmpeg -i \"{:}\" -af silencedetect=n={:}dB:d={:} -f null -".format(self.audio_file.as_posix(), sil_tolerance_db, sil_dur) # "-i" input_file, "-af" audio filters "astats" audio statistics, "-f" format (mandatory, set to null since not used) "-" output (not set since not used)
		args = shlex.split(ffmpeg_cmd)
		self.logger.debug("silence detection {:}".format(str(ffmpeg_cmd)))
		p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		# process ffmpeg output
		if(p.returncode == 0):
			sil_list = []
			ffmpeg_output = p.stderr.decode('utf-8')
			for line in ffmpeg_output.split('\n'):
				line = line.strip()
				if("silence_start" in line):
					sil_start = str(re.search("silence_start: (.+)", line).group(1))
				if("silence_end" in line):
					sil_end = str(re.search("silence_end: ([-]?[0-9]*\.?[0-9]+)", line).group(1))
				if("silence_duration" in line):
					sil_dur = str(re.search("silence_duration: ([-]?[0-9]*\.?[0-9]+)", line).group(1))
					if(sil_start is not None and sil_end is not None):
						sil_list.append([sil_start, sil_end, sil_dur])
		return sil_list
		
	def sample_audio(self, start, end):
		"""Extract the audio sample corresponding to the provided timestamps from reference audio file
		
		Parameters
		----------
		
		:param start: start time in s
		:type start: float
		
		:param end: end time in s
		:type end: int
		
		:return: sample audio
		:rtype: AudioSegment
		"""
		sample = self.audio_signal[start:end]
		return sample
		
	def save_audio_segment(self, segment, output_path):
		"""Save the provided audio segment at the provided location
		
		Parameters
		----------
		
		:param segment: audio to be saved
		:type segment: AudioSegment
		
		:param output_path: output file path
		:type output_path: Path
		"""
		segment.export(output_path.as_posix(), format='wav')
		return
		
	def save_file(self, output_file="output.wav"):
		"""Save the audio segment of the current audio file at the provided location
		
		Parameters
		----------
		
		:param output_path: output file path
		:type output_path: Path
		"""
		out_f = Path(self.output_path, output_file)
		file_handle = self.audio_signal.export(out_f.as_posix(), format="wav")
		return
		
	def sil_crawler(self, sil_list, output_path=None):
		"""Command line UI to go through the provided list of sounds and save sample if selected
		* Play silences
		* save if selected and stop
		CTRL+C: interruption of playback
		a: again, play current sample again
		s: save current sample
		p: pass, play next sample
		e: exit, stop (do not save)
		
		Parameters
		----------
		
		:param sil_list: list of timestamps (start_time, stop_time)
		:type sil_list: Path
		
		:param output_path: output file path to save the selected sample of silence to
		:type output_path: Path
		
		:return: status of the silence selection process: "exit", "next", "saved", None
		:rtype: string
		"""
		# set path from instantiated one if not provided
		if(output_path is None and self.output_path is not None):
			output_path=self.output_path
		# loop through every silences of the list
		sil_range = range(0, len(sil_list))
		sil_index=0
		while(True):
			sil = sil_list[sil_index]
		# for sil in sil_list:
			s_start = float(sil[0])*1000
			s_end = float(sil[1])*1000
			sil_signal = acp.sample_audio(s_start, s_end)
			action_k = "a"
			# UI
			while(action_k == "a"):
				# allows ctrl+c interruption of playback
				try:
					playback.play(sil_signal)
				except KeyboardInterrupt:
					pass
				action_k=input("play next detected silence ({}): n (default), play again: a, save: s, play next file: p, exit: e.".format(sil_index))
				if(action_k == "s"):	# save
					output_path=Path(output_path, self.audio_file.stem+".sil.wav")
					acp.save_audio_segment(sil_signal, output_path=output_path)
					return "saved"
				elif(action_k == "e"):	# exit
					return "exit"
				elif(action_k == "p"):	# pass
					return "next"
				else:
					sil_index+=1
					if(sil_index not in sil_range):
						sil_index=0
						self.logger.info("Back to first detected silence.")
		return
		
	def	correct_DC_offset(self, lib="ffmpeg"):
		"""Correct DC offset of current audio file
		
		
		Parameters
		----------
		
		:param lib: DC offset detection method: ffmpeg (preferred) or AudioSegment
		:type lib: string
		
		"""
		
		if(lib == "ffmpeg"):
			ret_stat_f = self.detect_DC_offect_ffmpeg()
		elif(lib == "AudioSegment"):
			ret_stat_f = self.detect_DC_offect_AudioSegment()
		if(ret_stat_f["overall"]["DC_offset"] != 0):
			try:	# stereo
				self.normalise_offset(ret_stat_f["2"]["DC_offset"], ret_stat_f["2"]["DC_offset"])
			except KeyError as e_num:	# mono
				if(str(e_num) == "'2'"):
					self.normalise_offset(ret_stat_f["1"]["DC_offset"])
		return
		
	def load_norm_silfind(self, audio_file):
		"""Process the given audio file
		* load file (see load_audio_file for constraints)
		* detect DC offset (FFMPEG)
		* correct DC offset
		* start UI to select silence
		
		Parameters
		----------
		
		:param audio_file: path to the audio file
		:type audio_file: Path
		
		:return: status of the silence selection process, see sil_crawler
		:rtype: string
		"""
		ret = None
		self.logger.info("Processing: {}".format(audio_file))
		self.load_audio_file(audio_file)
		#DC offset correction
		self.correct_DC_offset()
		# silence detection UI
		sil_list = self.detect_silence()
		self.logger.info("{} silences found".format(len(sil_list)))
		if(len(sil_list)>0):
			ret = self.sil_crawler(sil_list)
		return ret
		
	def load_corpus(self, corpus_path, recurive_search=False, audio_ext="wav"):
		"""Process the given corpus of audio
		* search files of interest (wav by default) located at the provided path
		
		Parameters
		----------
		
		:param corpus_path: path to the audio corpus
		:type corpus_path: Path
		
		:param recurive_search: search for audio files in subdirectories. Will only load files in root directory if set to False.
		:type recurive_search: boolean
		
		:param audio_ext: extention of the files to include in the search.
		:type recurive_search: string
		
		:return list of path to the files found in the search.
		"""
		self.logger.debug("Searching: {}".format(corpus_path))
		if(recurive_search):
			corpus_files = list(corpus_path.glob('**/*.'+audio_ext))
			remlist=[]
			for el in corpus_files:
				if self.output_path.as_posix() in el.as_posix():
					remlist.append(el)
			corpus_files = list(set(corpus_files) - set(remlist))
		else:
			corpus_files = list(corpus_path.glob('*.'+audio_ext))
		self.logger.info("Found {} files".format(len(corpus_files)))
		self.corpus = corpus_files
		self.corpus_path = corpus_path
		return corpus_files
		
	# def recursive_search_const(self, corpus_path, audio_ext):
		# exclude_dirs = [self.output_path]
		# corpus_files = list(corpus_path.glob('*.'+audio_ext))
		# for subdir in corpus_path.iterdir():
			# if( subdir.is_dir() and subdir not in exclude_dirs):
				# sublist_audio = self.recursive_search_const(subdir, audio_ext)
				# print(len(sublist_audio))
				# if(len(sublist_audio)>0):
					# corpus_files += sublist_audio
		# return corpus_files
		
	def silfind_corpus(self, corpus_path, recurive_search=False, audio_ext="wav", force_reprocess=False):
		"""Process the given corpus of audio
		* search wav files located at the provided path
		* apply load_norm_silfind on each file
		
		Parameters
		----------
		
		:param corpus_path: path to the audio corpus
		:type corpus_path: Path
		
		:param recurive_search: search for audio files in subdirectories. Will only load files in root directory if set to False.
		:type recurive_search: boolean
		
		:param audio_ext: extention of the files to include in the search. WILL ONLY WORK WITH "wav"
		:type recurive_search: string
		"""
		corpus_files = self.load_corpus(corpus_path, recurive_search, audio_ext)
		files_to_process = []
		# remove files that already have been cleaned
		if(force_reprocess is False):
			for file in corpus_files:
				cleaned_file=Path("{}/{}.clean{}".format(self.output_path, file.stem, file.suffix))
				if(not cleaned_file.exists()):	# do not process files that already have been cleaned
					files_to_process.append(file)
		else:
			files_to_process = corpus_files
			
		self.logger.info("Processing {} files".format(len(files_to_process)))
		# process each file
		for file in files_to_process:

			ret = self.load_norm_silfind(file)
			if(ret == "exit"):
				return
		return
	
	def noise_reduction_corpus(self, corpus_path=None, silence_path=None):
		"""Apply noise reduction on the given corpus of audio
		* search corresponding silence wav files
		* apply noise_reduction on each file
		
		Parameters
		----------
		
		:param corpus_path: path to the audio corpus
		:type corpus_path: Path
		
		:param silence_path: path to the silence files
		:type silence_path: Path
		"""
		if(corpus_path is None):
			corpus_path=self.corpus_path
		if(silence_path is None):
			silence_path=self.output_path
		silence_files = list(silence_path.glob('**/*.sil.wav'))
		# process each file
		for file in self.corpus:
			sil_file = Path(silence_path, file.stem+".sil.wav")
			if(sil_file in silence_files):
				ret = self.noise_reduction(file, sil_file)
			else:
				self.logger.warn("skipped - no silence file: {}".format(file.stem))
		return
		
	def noise_reduction(self, wav_file, sil_file=None, output_path=None, sensitivity=0.21):
		"""Apply noise reduction on the given corpus of audio
		* search corresponding silence wav files
		* extract noise profile for silence file
		* apply noise reduction on the main file
		
		Parameters
		----------
		
		:param wav_file: path to the audio file to apply NR to
		:type wav_file: Path
		
		:param sil_file: path to the corresponding silence file
		:type sil_file: Path
		
		:param output_path: path to the look for the silence file AND save "cleaned" file
		:type output_path: Path
		
		:param sensitivity: sensitivity in the sampling rates
		:type sensitivity: float
		
		:return: status of the noise reduction process, False if any step failed
		:rtype: boolean
		"""
		ret = False
		if(output_path is None):
			output_path=self.output_path
		if(sil_file is None):
			sil_file=Path(output_path, wav_file.stem+".sil.wav")
		# TODO? check sil_file exists
		cleaned_file = Path(output_path, wav_file.stem+".clean.wav")
		self.logger.info("Processing: {}".format(wav_file.stem))
		# generate noise profile
		noise_profile = "./noise.prof"
		sox_noise_profil = "sox \"{}\" -n noiseprof \"{}\"".format(sil_file, noise_profile)
		self.logger.debug("sox: noise profiling {:}".format(str(sox_noise_profil)))
		self.logger.info("sox: noise profiling")
		args = shlex.split(sox_noise_profil)
		p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		# check result of noise profiling
		if(p.returncode != 0):
			self.logger.error("sox: Profiling failed.")
			self.logger.debug("out: {}".format(p.stdout))
			self.logger.debug("err: {}".format(p.stderr))
		else:
			# Clean the noise samples from the audio stream:
			sox_clean = "sox \"{}\" \"{}\"  noisered \"{}\" {}".format(wav_file, cleaned_file, noise_profile, sensitivity)
			self.logger.debug("sox sox_clean {:}".format(str(sox_clean)))
			self.logger.info("sox: noise reduction")
			args = shlex.split(sox_clean)
			p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			if(p.returncode != 0):
				self.logger.error("sox: noise reduction failed.")
			else:
				ret = True
		return ret
		
	def quickstats_doc(self, doc):
		stat_array={}
		ffmpeg_cmd = "ffmpeg -i \"{:}\" -f null -".format(doc.as_posix()) # "-i" input_file, "-af" audio filters "astats" audio statistics, "-f" format (mandatory, set to null since not used) "-" output (not set since not used)
		args = shlex.split(ffmpeg_cmd)
		self.logger.debug("Retrieving stats {:}".format(str(args)))
		p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		if(p.returncode == 0):
			ffmpeg_output = p.stderr.decode('utf-8')
			for line in ffmpeg_output.split('\n'):
				line = line.strip()	# removes unwanted chars, e.g. '\r' on windows
				if("Duration:" in line):
					dura = str(re.search("Duration: ([^,]+),", line).group(1))
					dura_cut = dura.split(":")
					duration = datetime.timedelta(hours=int(dura_cut[0]), minutes=int(dura_cut[1]), seconds=float(dura_cut[2]))
					stat_array["duration"] = duration.total_seconds()
		return stat_array
		
	def quickstats(self):
		"""Retrive stats on audio corpus
		
		"""
		stat_array={}
		stat_array["global"]={}
		for doc in self.corpus:
			doc_id=doc.stem
			self.logger.info(doc_id)
			stat_array[doc_id] = self.quickstats_doc(doc)
			for stat in stat_array[doc_id]:
				if stat not in stat_array["global"]:
					stat_array["global"][stat] = []
				stat_array["global"][stat].append(stat_array[doc_id][stat])
		self.stat_array = stat_array
		total_docs = len(self.stat_array)-1	# number of docs - "global"
		self.logger.info("# docs {}".format(total_docs))
		
		for stat in stat_array["global"]:
			print(sum(stat_array["global"]["duration"]))
			print(len(self.stat_array["global"]))
		total_len = (sum(stat_array["global"]["duration"])/total_docs)
		m, s = divmod(total_len, 60)
		m_t, s_t = divmod(sum(stat_array["global"]["duration"]), 60)
		self.logger.info("# of docs {}".format(len(stat_array["global"]["duration"])))
		self.logger.info("total length {:d}'{:.0f}\"".format(int(m_t), s_t))
		self.logger.info("mean length {:d}'{:.0f}\"".format(int(m), s))
		return stat_array
		
		
	def quickplot(self):
		for item in self.stat_array["global"]:
			fig, ax = plt.subplots()
			ax.set_title(item)
			ax.set_xlabel("Corpus")
			ax = plt.boxplot(self.stat_array["global"][item])
			plt.savefig(item+".png")
		return
	
# main function (start the module)
if __name__ == "__main__":
	# configure processing
	corpus_path = Path(r"A:\corpus\audio")
	output_path = Path(r"A:\corpus\audio\out_sil")
	# file_path = Path("C:/work/datasets/INCA/Blood pressure demo.wav")	# mono
	file_path = Path("C:/work/datasets/INCA/f14467968.wav")	# stereo
	corpus_path_stats = Path(r"A:\corpus\audio")
	sil_tolerance_db_c=32	# -32dB
	sil_dur_c=0.4	# 0.4s
	# type of processing to do
	file_sw=False
	# corpus_sw=False
	corpus_sw=True
	corpus_sw_stats=False
	# generic configuration
	acp = AudioCorpusPreprocess()
	acp.set_output_path(output_path)
	acp.configure_silence_detection(sil_tolerance_db=sil_tolerance_db_c, sil_dur=sil_dur_c)
	
	##########################
	## process a specific file
	##########################
	if(file_sw):
		# load and clean
		acp.load_audio_file(file_path)
		acp.correct_DC_offset()
		acp.normalise_RMS()
		acp.save_file()
		
		# # search silences in a specific file
		# acp.load_norm_silfind(file_path)
		# acp.noise_reduction(file_path)
		
	
	##############################
	## search silences in a corpus
	##############################
	if(corpus_sw):
		acp.silfind_corpus(corpus_path)
		acp.noise_reduction_corpus()
	
	####################
	## stats of a corpus
	####################
	if(corpus_sw_stats):
		acp.load_corpus(corpus_path_stats, recurive_search=True, audio_ext="wav")
		acp.quickstats()
		acp.quickplot()


# TODO add 
	# cut ?



